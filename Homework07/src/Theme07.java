import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Theme07 {

    public static int[] selectionSort(int[] array) {               // Вставим сортировку выбором, как в вебинаре

        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minIndex = i;

            for (int j = i + 1; j < array.length; j++) {

                if (array[j] < min) {
                    min = array[j];
                    minIndex = j;
                }
            }
            int tmp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = tmp;
        }
        return array;
    }

    public static void findMinMatch(int[] array) {

        System.out.println("Проверка работы сортировки: " + Arrays.toString(array));

        int minRepeat = array.length;
        int repeatCount = 0;
        List<Integer> minRepeatNumbersList = new ArrayList<>();
        List<Integer> minRepeatCountList = new ArrayList<>();

        if (array.length == 1) {                   // Если в массиве будет только один элемент
            minRepeatNumbersList.add(array[0]);
            minRepeatCountList.add(repeatCount);
        }

        for (int i = 1; i < array.length; i++) {       // Для каждого элемента послед-сти, выполняем проверку совпадения

            if (array[i] == array[i - 1]) {
                repeatCount++;
            }
            if (array[i] != array[i - 1]) {            // Совпадения числа закончились
                if (repeatCount == minRepeat) {
                    minRepeatNumbersList.add(array[i - 1]);
                    minRepeatCountList.add(repeatCount);
                }
                if (repeatCount < minRepeat) {
                    minRepeatNumbersList.clear();
                    minRepeatCountList.clear();
                    minRepeat = repeatCount;
                    minRepeatNumbersList.add(array[i - 1]);
                    minRepeatCountList.add(repeatCount);
                }
                repeatCount = 0;
            }

            if (i == (array.length - 1)) {             // Проверка совпадений последнего числа в последовательности
                if (repeatCount <= minRepeat) {
                    if (repeatCount < minRepeat) {
                        minRepeatNumbersList.clear();
                        minRepeatCountList.clear();
                    }
                    minRepeatNumbersList.add(array[i]);
                    minRepeatCountList.add(repeatCount);
                }
            }
        }
        System.out.println("Числа с минимальным совпадением: ");

        for (Integer i: minRepeatNumbersList) {
            System.out.print(i + " ");
        }

        System.out.println("\nКол-во совпадений сответственно: ");

        for (Integer i: minRepeatCountList) {
            System.out.print(i + " ");
        }
    }

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        List<Integer> tmpList = new ArrayList<>();

        System.out.println("Введите число:\n(Справка: число -1 в последовательность не входит и останавливает ввод)");

        int number = console.nextInt();

        if (number != -1) {

            while (number != -1) {                           // принимаем, что -1 в последовательность не входит
                tmpList.add(number);
                number = console.nextInt();
            }
            System.out.println("Спасибо! Вы ввели количество чисел: " + tmpList.size());

            int[] newArray = new int[tmpList.size()];

            for (int i = 0; i < tmpList.size(); i++) {        // "Внутри цикла значение каждого элемента списка с индексом i
                newArray[i] = tmpList.get(i);                 // присваиваем ячейке массива с индексом i"
            }

            findMinMatch(selectionSort(newArray));
        }
        else {
            System.out.println("Вы ввели -1 сразу - ни одного числа нет для работы программы. Перезапустите программу!");
        }
    }
}
