package repository;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        System.out.println("Введите id");
        User filteredUser = usersRepository.findById(console.nextInt());

        if (filteredUser != null) {
            System.out.println(filteredUser.getId() + " " + filteredUser.getName() + " " + filteredUser.getAge() + " "
                               + filteredUser.isWorker());
            console.nextLine();

            System.out.println("Введите новое имя");
            filteredUser.setName(console.nextLine());

            System.out.println("Введите новый возраст");
            filteredUser.setAge(console.nextInt());

            usersRepository.update(filteredUser);

        } else {
            System.out.println("Пользователь не найден");
        }
    }
}
