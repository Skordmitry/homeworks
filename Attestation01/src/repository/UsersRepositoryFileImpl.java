package repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        Reader reader = null;                                       // объявили переменные для доступа
        BufferedReader bufferedReader = null;

        try {
            reader = new FileReader(fileName);                      // создали читалку на основе файла
            bufferedReader = new BufferedReader(reader);            // создали буферизированную читалку

            String line = bufferedReader.readLine();                // прочитали строку

            while (line != null) {                                  // пока к нам не пришла нулевая строка
                String[] parts = line.split("\\|");           // разбиваем её по | символу
                int id = Integer.parseInt(parts[0]);                // берем id, помещаем в ячейку массива
                String name = parts[1];                             // берем имя, помещаем в ячейку массива
                int age = Integer.parseInt(parts[2]);               // берем возраст
                boolean isWorker = Boolean.parseBoolean(parts[3]);  // берем сатус о работе
                User newUser = new User(id, name, age, isWorker);   // создаём нового человека
                users.add(newUser);                                 // добавляем его в список
                line = bufferedReader.readLine();                   // считываем новую строку
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        } finally {
            if (reader != null) {                                   // этот блок выполнится точно
                try {
                    reader.close();                                 // пытаемся закрыть ресурсы
                } catch (IOException ignore) {}
            }

            if (bufferedReader != null) {
                try {
                    bufferedReader.close();                         // пытаемся закрыть ресурсы
                } catch (IOException ignore) {}
            }
        }
        return users;
    }

    @Override
    public User findById(int id) {
        for (User user : findAll()) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void update(User user) {
        List<User> newList = findAll();
        Writer writer = null;                                       // объявили переменные для доступа
        BufferedWriter bufferedWriter = null;

        try {
            writer = new FileWriter(fileName, false);        // создали записывалку для файла с перезаписью
            bufferedWriter = new BufferedWriter(writer);            // создали буферизированную записывалку

            for (User user1 : newList) {                            // читаем из листа, условие по id, пишем в буфер
                if (user1.getId() == user.getId()) {
                    bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
                } else {
                    bufferedWriter.write(user1.getId() + "|" + user1.getName() + "|" + user1.getAge() + "|" + user1.isWorker());
                }
                bufferedWriter.newLine();                           // переходим на другую строку
            }
            bufferedWriter.flush();                                 // выгружаем из буфера в файл
        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }

            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
        }
    }
}
