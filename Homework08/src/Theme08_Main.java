import java.util.Scanner;

public class Theme08_Main {

    public static void main(String[] args) {
        Human[] humans = new Human[10];
        Scanner console = new Scanner(System.in);

        for (int i = 0; i < humans.length; i++) {
            System.out.println("Введите имя");

            String humanName = console.nextLine();

            System.out.println("Введите вес");

            while (!console.hasNextDouble()) {
                System.out.println("Это не вещественное число, повторите ввод");

                System.out.println("Введите вес");

                console.nextLine();
            }
            double humanWeight = console.nextDouble();
            console.nextLine();
            humans[i] = new Human();
            humans[i].setName(humanName);
            humans[i].setWeight(humanWeight);

        }
        System.out.println("До сортировки массива по значению веса");

        for (Human human : humans) {
            System.out.println("Вес: " + human.getWeight() + " Имя: " + human.getName());
        }

        for (int i = 0; i < humans.length; i++) {
            double min = humans[i].getWeight();
            int minIndex = i;

            for (int j = i + 1; j < humans.length; j++) {

                if (humans[j].getWeight() < min) {
                    min = humans[j].getWeight();
                    minIndex = j;
                }
            }
            Human tmp = humans[i];
            humans[i] = humans[minIndex];
            humans[minIndex] = tmp;
        }
        System.out.println("После сортировки массива по значению веса");

        for (Human human : humans) {
            System.out.println("Вес: " + human.getWeight() + " Имя: " + human.getName());
        }
    }
}
