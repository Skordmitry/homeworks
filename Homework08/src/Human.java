public class Human {

    private String name;     // Задаём имя объектов
    private double weight;   // Задаём вес объектов

   public void setName(String name) {
       if (name.equals("")) {
           name = "Безымянный";
       }
       this.name = name;
   }

   public void setWeight(double weight) {
       if (weight < 0.0 || weight > 300.0) {
           weight = 0.0;
       }
       this.weight = weight;
   }

    public String getName() {
        return this.name;
    }

    public double getWeight() {
        return this.weight;
    }
}
