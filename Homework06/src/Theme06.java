import java.util.Arrays;

public class Theme06 {

    /**
     * Возвращает индекс входного числа в массиве
     * @param array Массив, который поступает в метод в качестве входящего параметра
     * @param number Число, для которого нужно получить индекс в массиве
     * @return Индекс входного числа в массиве. Если числа в массиве нет, то фиксированное число -1
     */
    public static int getArrayIndex(int[] array, int number) {

        for (int i = 0; i < array.length; i++) {

            if (array[i] == number) {
                return i;
            }
        }
        return -1;
    }

    public static void movesNumbersOfArray(int[] array) {

        for (int i = array.length-1; i > 0; i--) {

            for (int j = 0; j < i; j++) {

                if (array[j] == 0) {
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        int[] anyArray = {1, 2, 3, 4, 5};
        int anyNumber = 3;
        int indexInArray = getArrayIndex(anyArray, anyNumber);

        System.out.println(indexInArray);

        int[] specifiedArray = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        movesNumbersOfArray(specifiedArray);
    }
}
