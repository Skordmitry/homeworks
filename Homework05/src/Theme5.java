import java.util.Scanner;

public class Theme5 {

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        if (console.hasNextInt()) {
            int a = console.nextInt();          // На случай, если пользователь введёт не целое число или строковое значение.

            if (a != -1) {                      // На случай, если пользователь сразу введёт -1.
                int minDigit = Math.abs(a);     // На случай, если пользователь сразу введёт отрицательное число, а также для случая, когда вводится от 0 до 9, а после него -1.

                while (a != -1) {
                    a = Math.abs(a);            // Чтобы алгоритм работал, независимо от знака введённого числа.

                    while (a != 0) {
                        int lastDigit = a % 10;

                        if (minDigit > lastDigit) {
                            minDigit = lastDigit;
                        }
                        a = a / 10;
                    }
                    a = console.nextInt();
                }
                System.out.println(minDigit);
            } else {
                System.out.println("Вы не ввели ни одного числа, входящего в последовательность,"
                        + " крайнее число -1 в неё не входит, Вы можете запустить программу ещё раз");
            }
        } else {
            System.out.println("Извините, но это явно не целое число. Перезапустите программу и попробуйте снова!");
        }
    }
}
